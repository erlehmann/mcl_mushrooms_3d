--[[

mcl_mushrooms_3d – Minetest mod to render small mushrooms as 3D objects.
Copyright © 2022  Nils Dagsson Moskopp (erlehmann)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

]]--

local node_box_mushroom_brown = {
	type = "fixed",
	fixed = {
		{ -1/16, -8/16, -1/16,  1/16,  -5/16,  1/16 },
		{ -4/16, -5/16, -4/16,  4/16,  -2/16,  4/16 },
	},
}

local node_box_mushroom_red = {
	type = "fixed",
	fixed = {
		{ -1/16, -8/16, -1/16,  1/16,  -5/16,  1/16 },
		{ -4/16, -5/16, -4/16,  4/16,  -2/16,  4/16 },
		{ -3/16, -2/16, -3/16,  3/16,   0/16,  3/16 },
	},
}

local get_node_box = function(node_name, node_def)
	local node_box
	if string.match(node_name, "mcl_mushrooms:mushroom_brown") then
		node_box = node_box_mushroom_brown
	elseif string.match(node_name, "mcl_mushrooms:mushroom_red") then
		node_box = node_box_mushroom_red
	end
	return node_box
end

local get_tiles = function(node_name)
	local tiles
	if "mcl_mushrooms:mushroom_brown" == node_name then
		return {
			"mcl_mushrooms_mushroom_block_skin_brown.png",
			"mcl_mushrooms_mushroom_block_inside.png",
			"mcl_mushrooms_mushroom_block_skin_brown.png^[lowpart:18.75:mcl_mushrooms_mushroom_block_skin_stem.png",
		}
	elseif "mcl_mushrooms:mushroom_red" == node_name then
		return {
			"mcl_mushrooms_mushroom_block_skin_red.png",
			"mcl_mushrooms_mushroom_block_inside.png",
			"mcl_mushrooms_mushroom_block_skin_red.png^[lowpart:18.75:mcl_mushrooms_mushroom_block_skin_stem.png",
		}
	end
	return tiles
end

local add_mushrooms_3d = function()
	for node_name, node_def in pairs(minetest.registered_nodes) do
		local node_box = get_node_box(node_name, node_def)
		local tiles = get_tiles(node_name)
		if (
			nil ~= node_box and
			nil ~= tiles
		) then
			minetest.override_item(
				node_name,
				{
					drawtype = "nodebox",
					node_box = node_box,
					selection_box = node_box,
					tiles = tiles,
				}
			)
		end
	end
end

minetest.register_on_mods_loaded(add_mushrooms_3d)
